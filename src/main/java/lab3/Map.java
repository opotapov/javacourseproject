package lab3;

public class Map {

    private Cell firstCell;

    private static Cell getNextCell(Cell currentCell) {
        if (currentCell == null)
            return null;

        if (currentCell.getRightCell() != null) {
            return currentCell.getRightCell();
        }

        while (currentCell.getLeftCell() != null) {
            currentCell = currentCell.getLeftCell();
        }

        currentCell = currentCell.getLowerCell();

        return currentCell;
    }

    private static Cell getFirstCell (Cell currentCell) {
        if (currentCell == null) {
            return null;
        }

        while (currentCell.getUpperCell() != null) {
            currentCell = currentCell.getUpperCell();
        }

        while (currentCell.getLeftCell() != null) {
            currentCell = currentCell.getLeftCell();
        }

        return currentCell;
    }

    // method for map building
    public void buildMap (String data) {

        // creating cells for first element before start of map building
        Cell leftCell = null;
        Cell upperCell = null;

        // split lines in a array
        for (String line : data.split("\n")) {

            // get character in a line
            for (byte character : line.getBytes()) {

                // If the current character is '1', the cell type is 'block'. Otherwise it's 'node'.
                Cell currentCell = new Cell(character == '1' ? CellTypes.Block : CellTypes.Node);

                // If the left cell is not null, transition cell should be set between left cell and current cell
                if (leftCell != null) {
                    Cell transition = new Cell(CellTypes.Transition);
                    transition.setLeftCell(leftCell);
                    transition.setRightCell(currentCell);

                    leftCell.setRightCell(transition);

                    currentCell.setLeftCell(transition);
                }

                // If the upper cell is not null, transition cell should be set between upper cell and current cell
                if (upperCell != null) {
                    Cell transition = new Cell(CellTypes.Transition);
                    transition.setUpperCell(upperCell);
                    transition.setLowerCell(currentCell);

                    upperCell.setLowerCell(transition);

                    currentCell.setUpperCell(transition);
                }

                // if both upper cell and left cell are not null, upper left cell should be empty
                if (upperCell != null && leftCell != null) {
                    Cell upperLeftCell = new Cell(CellTypes.Empty);

                    if (currentCell.getLeftCell() != null) {
                        upperLeftCell.setLowerCell(currentCell.getLeftCell());
                        currentCell.getLeftCell().setUpperCell(upperLeftCell);
                    }

                    if (currentCell.getUpperCell() != null) {
                        upperLeftCell.setRightCell(currentCell.getUpperCell());
                        currentCell.getUpperCell().setLeftCell(upperLeftCell);
                    }

                    if (upperCell.getLeftCell() != null) {
                        upperLeftCell.setUpperCell(upperCell.getLeftCell());
                        upperCell.getLeftCell().setLowerCell(upperLeftCell);
                    }

                    if (leftCell.getUpperCell() != null) {
                        upperLeftCell.setLeftCell(leftCell.getUpperCell());
                        leftCell.getUpperCell().setRightCell(upperLeftCell);
                    }
                }

                // updating the reference
                if (upperCell != null && upperCell.getRightCell() != null) {
                    upperCell = upperCell.getRightCell().getRightCell();
                }

                // updating the reference
                leftCell = currentCell;
            }

            // move current position to beginning of line
            while (leftCell != null && leftCell.getLeftCell() != null) {
                leftCell = leftCell.getLeftCell();
            }
            upperCell = leftCell;

            // this reference is set to null to avoid overwriting the links we made before
            leftCell = null;
        }

        // first element of map should be saved as an entry point
        this.firstCell = getFirstCell(upperCell);
    }

    public void updateAll(CellTypes targetType) {
        Cell currentCell = firstCell;

        while (currentCell != null) {
            currentCell.setCellType(targetType);
            currentCell = getNextCell(currentCell);
        }
    }

    public void printMap() {
        Cell currentCell = firstCell;

        while (currentCell != null) {
            currentCell.printCell();
            currentCell = getNextCell(currentCell);
        }
    }

}

