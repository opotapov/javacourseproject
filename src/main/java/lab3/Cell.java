package lab3;

public class Cell {
    private CellTypes type;

    private Cell upperCell;
    private Cell lowerCell;
    private Cell rightCell;
    private Cell leftCell;

    public Cell(CellTypes type) {
        this.type = type;
    }

    public Cell getUpperCell() {
        return upperCell;
    }

    public void setUpperCell(Cell upperCell) {
        this.upperCell = upperCell;
    }

    public Cell getLowerCell() {
        return lowerCell;
    }

    public void setLowerCell(Cell lowerCell) {
        this.lowerCell = lowerCell;
    }

    public Cell getRightCell() {
        return rightCell;
    }

    public void setRightCell(Cell rightCell) {
        this.rightCell = rightCell;
    }

    public Cell getLeftCell() {
        return leftCell;
    }

    public void setLeftCell(Cell leftCell) {
        this.leftCell = leftCell;
    }

    public boolean isBlock() {
        return type == CellTypes.Block;
    }

    public void printCell() {
        switch (type) {
            case Empty:
                System.out.print("  ");
                break;

            case Block:
                System.out.print("▦ ");
                break;

            case Node:
                System.out.print("+ ");
                break;

            case Transition:
                System.out.print("· ");
                break;
        }

        if (rightCell == null) {
            System.out.print("\n");
        }
    }

    public void setCellType (CellTypes newType) {
        switch (type) {
            case Empty:
                if (newType == CellTypes.Empty && (

                        // checks if this cell has 'block' neighbors at opposing sides
                        (rightCell == null || rightCell.isBlock()) && (leftCell == null || leftCell.isBlock()) ||
                                (upperCell == null || upperCell.isBlock()) && (lowerCell == null || lowerCell.isBlock())
                )) {
                    this.type = CellTypes.Block;
                }
                break;

            case Transition:
                if (newType == CellTypes.Transition && (

                        // checks if there is at least one 'block' neighbor
                        (leftCell != null && leftCell.isBlock()) ||
                                (rightCell != null && rightCell.isBlock()) ||
                                (upperCell != null && upperCell.isBlock()) ||
                                (lowerCell != null && lowerCell.isBlock())
                )) {
                    this.type = CellTypes.Block;
                }
                break;
        }

    }
}

