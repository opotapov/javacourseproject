package lab3;
// the lab 3 was done with analysis based on code presented by Anatoly


public class Main {

    public static void main(String[] args) {
        Map map = new Map();

        String str = "00100011\n01001001\n00011100\n01011000\n00100000";

        System.out.println("Строка:");
        System.out.println(str);

        System.out.println("Прогон 0:");
        map.buildMap(str);
        map.printMap();

        System.out.println("Прогон 1:");
        map.updateAll(CellTypes.Transition);
        map.printMap();

        System.out.println("Прогон 2:");
        map.updateAll(CellTypes.Empty);
        map.printMap();
    }
}

