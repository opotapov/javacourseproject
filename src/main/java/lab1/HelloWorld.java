/* A class that prints "Hello, World!"
 *
 * @autor Oleg Potapov
 * @version 1.0
 */

package lab1;

public class HelloWorld {
    public static void printHelloWorld (String[] args)
    {
        System.out.println("Hello, World!");
    }
}