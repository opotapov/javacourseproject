/* A class for drawing map of array
 *
 * @autor Oleg Potapov
 * @version 1.0
 */

package lab3_initial;

import java.lang.String;
import java.lang.StringBuilder;

public class MapOfArray {

    private String str;

    public MapOfArray(String str) {
        this.str = str;
    }

    /*Метод для получения "карты" прогоном 0*/
    public static StringBuilder getMap0(String str) {
        int size = str.indexOf("\n") * 2; //размер карты, исходя из количества символов первой строки до переноса
        int length = str.length();
        int count = 0;
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < length; ++i) {
            switch (str.charAt(i)) {
                case '0':
                    result.append("+ . ");
                    break;
                case '1':
                    result.append("▦ . ");
                    break;
                case '\n':
                    result.deleteCharAt(result.length() - 2);
                    result.append("\n");
                    break;
            }
            if (str.charAt(i) == '\n') {
                for (int y = 0; y < size; ++y) {
                    if (y % 2 == 0)
                        result.append(". ");
                    else {
                        result.append("  ");
                    }

                }
                result.append("\n");
            }
        }
        result.deleteCharAt(result.length() - 2);
        return result;
    }

    /*Метод для получения "карты" прогоном 1*/
    public static StringBuilder getMap1(StringBuilder map0) {
        StringBuilder result = map0;
        int exception = -1; // индекс измененного следующего символа, который нельзя анализировать
        int strcount = 0; // номер текущей строки
        int fsize = map0.indexOf("\n") + 1; // количество символов до изменения символа в строке ниже текущей
        int bsize = map0.indexOf("\n") + 2; // количество символов до изменения символа в строке ниже текущей
        int length = map0.length(); // длина строки
        for (int i = 0; i < length; ++i) {
            switch (map0.charAt(i)) {
                case '▦':
                    if (i != exception && i - 2 > -1 && result.charAt(i - 2) != ' ' && result.charAt(i - 2) != '\n') {
                        result.setCharAt(i - 2, '▦');
                    }
                    if (i != exception && i + 2 < length && result.charAt(i + 2) != ' ' && result.charAt(i + 2) != '\n') {
                        result.setCharAt(i + 2, '▦');
                        exception = i + 2;
                    }
                    if (i != exception && i + fsize < length && result.charAt(i + fsize) != ' ' && result.charAt(i + fsize) != '\n') {
                        result.setCharAt(i + fsize, '▦');
                    }
                    if (i != exception && i - bsize > -1 && result.charAt(i - bsize) != ' ' && result.charAt(i - bsize) != '\n') {
                        result.setCharAt(i - bsize, '▦');
                    }
                    break;
                case '\n':
                    if (strcount % 2 == 0) {
                        i = i + fsize;
                        strcount++;
                    } else {
                        i++;
                        strcount++;
                    }
                    break;
            }
        }
        return result;
    }

    /*Метод для получения "карты" прогоном 2*/
    public static StringBuilder getMap2(StringBuilder map1) {
        StringBuilder result = map1;
        int strcount = 0; // номер текущей строки
        int size = map1.indexOf("\n") + 1;
        int length = map1.length(); // длина строки
        for (int i = 0; i < length; ++i) {
            switch (map1.charAt(i)) {
                case ' ':
                if (i - 2 > -1 && i + 2 < length && result.charAt(i - 2) == '▦' && result.charAt(i + 2) == '▦') {
                        result.setCharAt(i, '▦');
                }
                break;
            }

        }
        return result;
    }
}
